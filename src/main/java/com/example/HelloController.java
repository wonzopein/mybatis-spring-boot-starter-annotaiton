package com.example;

import com.example.domain.City;
import com.example.mapper.CityMapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description
 *
 * @author wonzopein
 * @version 2015. 11. 29.
 */
@RestController
public class HelloController {

    @Autowired
    CityMapper cityMapper;

    @RequestMapping("/")
    String hello(){

        String result = "Hello!";

        System.out.println(cityMapper.findByState("CA"));
        System.out.println(cityMapper.findByState("AA"));
        System.out.println(cityMapper.findByState("SA"));

        return result;
    }


}
